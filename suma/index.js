
const express = require('express')
const bodParser = require('body-parser')
let cors = require('cors')

const app = express()
app.use(bodParser.json({limit:'50mb', extended:true}))
app.use(bodParser.urlencoded({limit:'50mb', extended:true}))
app.use(cors())

app.listen(3000, () => {
 console.log("El servidor está inicializado en el puerto 3000");
});


app.post('/suma', async (req, res) => {
    const{n1,n2}=req.body.prueba;
    const resultado= n1+ n2;
    res.send("El resultado de la suma es: "+resultado)
  })

app.post('/resta', async (req, res) => {
    const{n1,n2}=req.body.prueba;
    const resultado= n1 - n2;
    res.send("El resultado de la resta es: " + resultado)
  })