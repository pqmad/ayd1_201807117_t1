
const express = require('express')
const bodParser = require('body-parser')
let cors = require('cors')

const app = express()
app.use(bodParser.json({limit:'50mb', extended:true}))
app.use(bodParser.urlencoded({limit:'50mb', extended:true}))
app.use(cors())

app.listen(4000, () => {
 console.log("El servidor está inicializado en el puerto 4000");
});

app.get('/info', async (req, res) => {
    res.send("Madeline Ariana Pérez Quiñónez  --->  201807117")
  })

  app.get('/adios', async (req, res) => {
    res.send("Termino el flujo...")
  })
